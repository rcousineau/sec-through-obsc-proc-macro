extern crate proc_macro;

use crate::proc_macro::TokenStream;
use base64;
use proc_macro2::TokenStream as TokenStream2;
use quote::quote;
use std::str::FromStr;
use syn::{parse_macro_input, Expr, ItemFn};

#[proc_macro_attribute]
pub fn sec_through_obsc(attr: TokenStream, item: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(item as ItemFn);
    let attrs = ast.attrs.into_iter();
    let vis = ast.vis;
    let sig = ast.sig;

    let raw_body = match base64::decode(&attr.to_string()) {
        Ok(decoded) => match String::from_utf8(decoded) {
            Ok(s) => match TokenStream::from_str(&s) {
                Ok(tokens) => tokens,
                Err(e) => panic!("obscurity token does not decode into valid tokens! {:?}", e),
            },
            Err(e) => panic!("obscurity token does not decode to valid UTF-8! {}", e),
        },
        Err(e) => panic!("failed to decode obscurity token! {}", e),
    };

    let body = Expr::Verbatim(TokenStream2::from(raw_body));

    let gen = quote! {
        #(
            #attrs
        )*
        #vis #sig {
            #body
        }
    };

    TokenStream::from(gen)
}
