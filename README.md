# `sec_through_obsc` attribute proc macro

This is a Cargo [Workspace][workspaces] that contains:
 * `sec_through_obsc` proc-macro crate
 * `sample` binary crate

This is part of my exploration into Rust Macros. The full slide deck can be
found [here][hello-rust-macros].

### Usage

1. Identify some functionality you want to obscure (example below: `(n * n) + 1`)
2. Serialize this functionality into a base-64 string

```sh
$ echo '(n * n) + 1' | base64
KG4gKiBuKSArIDEK
```

3. Modify the body of the function which contains this functionality (example below: `n * n`)
4. Add `#[sec_through_obsc(<token>)]` attribute to the function, passing the base-64 string

### Example

see: sample/src/main.rs

```rs
use sec_through_obsc::sec_through_obsc;

#[sec_through_obsc(KG4gKiBuKSArIDEK)]
fn square(n: i32) -> i32 {
    n * n
}

fn main() {
    dbg!(square(2));
    dbg!(square(3));
}
```

[workspaces]: https://doc.rust-lang.org/book/ch14-03-cargo-workspaces.html
[hello-rust-macros]: https://gitlab.com/rcousineau/hello-rust-macros
