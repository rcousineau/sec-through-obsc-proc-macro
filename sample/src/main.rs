use sec_through_obsc::sec_through_obsc;

#[sec_through_obsc(KG4gKiBuKSArIDEK)]
fn square(n: i32) -> i32 {
    n * n
}

fn main() {
    dbg!(square(2));
    dbg!(square(3));
}
